"""recursive function prints the rhyme TEN GREEEN BOTTLES"""
def recursive_function():
	bottles = 10
	while bottles > 0:
		print ( bottles , " green bottles hanging on the wall , " )
		print ( bottles , " green bottles hanging on the wall , " )
		print ( " And if one green bottle should accidentally fall , " )
		print ( " There ’ ll be " , bottles -1 , " green bottles hanging on the wall " )
		print ()
	bottles = bottles-1
	recursive_function()
recursive_function()
