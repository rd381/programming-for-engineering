"""program generates random number and compares it to guessed number"""
import random
number = int(random.random()*100)+1 #generates random number#
"""asks user for a guess number"""
guess = 0
while guess != number:
	guess = int(input("Have a guess:"))
	if guess < number:
			print("Your guess is too low")
	if guess > number:	
			print("Your guess is too high")

else:	
	print("Your guess is right")
