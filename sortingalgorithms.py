
def MergeSort(L):
	if len(L) < 2: 
		return L
	else:
		
			half = len(L)//2
			Lleft = L[:half]
			Lright = L[half:]
			Lleft = MergeSort(L[:half])
			Lright = MergeSort(L [half:])
			return Merge(L,Lleft, Lright)
	
		
def Merge(L,Lleft,Lright):
	L=[]	
	while len(Lleft)!= 0 and len(Lright) !=0:
		
		if Lleft[0] < Lright[0]:
			L.append(Lleft.pop(0))
		else:
			L.append(Lright.pop(0))
	
	if len(Lleft) != 0:
		L.extend(Lleft)
	else:
		L.extend(Lright)
	return L
L = [2,3,1,8,5]
print(MergeSort(L))	


	
