""" This program calculates the first 20 values of the Fibonacci sequence"""

L = [0,1]
for n in range(20):
	value = L[n]+L[n+1]
	L.append(value)
print(L)