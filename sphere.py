import numpy as np
import matplotlib . pyplot as plt


def sphere(gene):
	"""
	Return the fitness of the solution x - sum the bits.
	"""
	total = 0
	for i in gene:
			total+=i**2
	return total


def copy(gene):
	"""
	Return a new copy of the solution x.
	"""
	return gene.copy()


def mutate(gene):
	"""
	Flip one of the bits in the solution x.
	"""
	while (True):
		D = len(gene)
		z = np.random.randn()* 0.1
		index = np.random.randint(D)
		#print(type(gene[index]))
		gene[index] = gene[index] + z
		if gene[index] > 1 and gene[index] < -1:
			break
		else:	
			return gene
def ea(n_gens):
	"""
	Optimise the onemax problem for n_gens generations.
	"""
	D = 10
	history=[]
	parent = np.ones(D)
	
	parent_fitness = sphere(parent)

	for i in range(n_gens):
		child = copy(parent)
		child = mutate(child)
		child_fitness = sphere(child)

		if child_fitness < parent_fitness:
			parent = child
			parent_fitness = child_fitness
		else:
			# Nothing to do - the parent is fitter than the
			# child so keep the current parent for the next
			# generation.
			pass
		history.append(parent_fitness)
	# Finished optimisation - return the parent (and its fitness).
	return parent, parent_fitness, history

	
	
	for i in range(n_gens):
		child = copy(parent)
		child = mutate(child)
		child_fitness = sphere(child)

		if child_fitness > parent_fitness:
			parent = child
			parent_fitness = child_fitness
		else:
			# Nothing to do - the parent is fitter than the
			# child so keep the current parent for the next
			# generation.
			pass
	
	# Finished optimisation - return the parent (and its fitness).
		

if __name__ == "__main__":
	solution, fitness, history = ea(20)
	print(solution, fitness, history)


	repeats = 30
	fitnesses = []

	for repeat in range(repeats):
		solution, fitness, history = ea(100)
		fitnesses.append(fitness)

	# Convert the list to a numpy array
	fitnesses = np.array(fitnesses)
	print("Mean fitness is", fitnesses.mean())
	for repeat in range(10,200,10):
		plt.plot(history)
		plt.ylabel(history)
		plt.show()